import React, { Component } from 'react';
import Joi from 'joi-browser';
import Form from './common/form';


class NewMovie extends Form {
    state = {
        data: { title: '', genre: '', stock: '', rate: '' },
        errors: {}
    };

    schema = {
        title: Joi
            .string()
            .required()
            .label('Title'),
        genre: Joi
            .string()
            .required()
            .label('Genre'),
        stock: Joi
            .number()
            .required()
            .min(1)
            .integer()
            .max(100)
            .label('Stock'),
        rate: Joi
            .number()
            .required()
            .min(1)
            .max(10)
            .label('Rate')

    };

    doSubmit = () => {
        console.log('Submitted');
    };


    render() {
        return (
            <div>
                <h1>Add New Movies</h1>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput('title', 'Title')}
                    {this.renderInput("genre", "Genre")}
                    {this.renderInput('stock', 'Stock')}
                    {this.renderInput('rate', 'Rate')}
                    {this.renderButton("Save")}
                </form>
            </div>
        );
    }
}

export default NewMovie;